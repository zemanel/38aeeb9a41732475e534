# Tmux

* https://github.com/tmux-plugins/tmux-resurrect
* https://mutelight.org/caps-lock
* https://github.com/jooize/tmux-powerline-theme

# UI

* https://github.com/powerline/powerline

http://stackoverflow.com/questions/5609192/how-to-set-up-tmux-so-that-it-starts-up-with-specified-windows-opened

# IPython

open -a /usr/local/bin/jupyter --args qtconsole

http://blog.yhat.com/posts/rodeo-native.html
